% Paper template for TAR 2022
% (C) 2014 Jan Šnajder, Goran Glavaš, Domagoj Alagić, Mladen Karan
% TakeLab, FER

\documentclass[10pt, a4paper]{article}

\usepackage{tar2022}

\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage[inline]{enumitem}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{placeins}
\usepackage{stfloats}
\usepackage{caption}
\captionsetup[figure]{font=small}

\newcommand*{\SuperScriptSameStyle}[1]{%
  \ensuremath{%
    \mathchoice
      {{}^{\displaystyle #1}}%
      {{}^{\textstyle #1}}%
      {{}^{\scriptstyle #1}}%
      {{}^{\scriptscriptstyle #1}}%
  }%
}

\usepackage{hyperref}
 \hypersetup{
    colorlinks=true,
    linkcolor=black,     
    urlcolor=black,
    citecolor=black
}

\newcommand*{\oneS}{\SuperScriptSameStyle{*}}

\title{Don't Forget to Pair All of Your Paragraphs!}

\name{Tin Ferković, Ivan Martinović, Marko Andreis} 

\address{
University of Zagreb, Faculty of Electrical Engineering and Computing\\
Unska 3, 10000 Zagreb, Croatia\\ 
\texttt{\{tin.ferkovic, ivan.martinovic, marko.andreis\}@fer.hr}\\
}
         
\abstract{ 
In this paper, we solve task 2 of the PAN at CLEF 2021 Author Style Change Detection competition, where the main goal was to identify whether an author change occurred between paragraphs, which seems to be a challenging task. We approach this problem as a binary classification by feeding BERT two paragraphs at a time. Additionally, we explore the impact of extending and cleaning the dataset on performance metrics. We measure the time needed and state the number of parameters for fine-tuning different parts of the model. Finally, we inspect the model's performance on the unseen data and state some of its drawbacks through examples.
}

\begin{document}

\maketitleabstract

\section{Introduction}

Author style is a unique way an author writes. It is sometimes in our interest to identify whether a text is written by one or multiple authors. A reason for that could be plagiarism detection or simply identifying all the authors' change positions in order to act differently towards the texts from different authors. While plagiarism detection algorithms usually work on identifying exact matches between the document parts, author style change detection is focused on capturing the author's unique style and recognizing a change in such. This remains a difficult task, due to the complex nature of humans and their writing styles.

PAN at CLEF (Conference and Labs of the Evaluation Forum) 2021 competition's topic was the detection of author style change on the StackExchange posts.\footnote{\url{https://pan.webis.de/clef21/pan21-web/style-change-detection.html}}
    The competition was split into three tasks,
    \begin{enumerate*}[label=(\arabic*), itemjoin={{, }}, itemjoin*={{, and }}]
    \item binary classify if the text has a single or multiple authors
    \item identify whether an author change occurred in each of the paragraph transitions
    \item assign an author label to each of the paragraphs.
    \end{enumerate*}
    
In this paper, we focus on identifying an author change given two paragraphs using BERT \citep{bert-paper}. Among last year's submissions, we achieve the best results on task 2, which we focused on. We primarily focus on the experiments with the extension of the dataset, preprocessing, and fine-tuning different parts of the model architecture. We perform multiple non-parametric statistical tests regarding the observations and our prior hypotheses. Then we take a look at how the model performs on certain test examples. Although an author's style intuitively seems to be mostly oriented around the syntax, and BERT is designed to capture a semantic context, it proved to be effective in identifying the style change. The entire code is available on GitLab.\footnote{\url{https://gitlab.com/helenic/author-style-change-detection-clef2021}}

\section{Related Work}
Author style change detection has been a PAN at CLEF task since 2017, even though author identification has been examined in the competition since 2011. The tasks varied throughout the years.

PAN at CLEF 2019 had only the task of detecting the number of authors in a document. \citet{zuo2019style} based their work on the feed-forward neural networks using clustering on the features extracted from the previous year's competition \citep{zlatkova2018ensemble}, some of which are:
\begin{enumerate*}[label=(\roman*), itemjoin={{, }}, itemjoin*={{, and }}]
\item repetition of N-grams
\item contracted word forms, e.g. \textit{I will (I'll)}
\item quotation marks
\item vocabulary richness
\item readability.
\end{enumerate*}
We state these because they are used by multiple other participants in later years.

\citet{iyer2020style} approached the problem using BERT. They generate embedding on the sentence level by summing the embeddings of the last 4 layers, then summing them again across the time dimension. Finally, they add the embeddings of all sentences in two consecutive paragraphs and divide it by the total number of sentences in these two paragraphs. There is a Random Forest classifier at the top of the architecture.

\citet{nath2021style} fed word embeddings of each paragraph into a bidirectional LSTM (or GRU). The similarity of these two outputs is used in a Siamese learning fashion.

Last year's work of \citet{strom2021multi} is a combination of previously mentioned word embeddings \citep{iyer2020style} and text features \citep{zlatkova2018ensemble}. They found that training on both BERT embeddings and text features showed no improvement upon solely using text features. Thus, they trained separate classifiers on BERT embeddings and text features and then combined them using a stacking ensemble architecture.

Finally, the last year's winning team's approach was to use BERT with a feed-forward neural network \citep{zhang2021style}. The exact approach wasn't explained thoroughly, so we cannot make further comparisons to ours. However, the performance gap suggests that there are some key differences.

Our architecture differs from the previous ones in a way that we're feeding BERT with both paragraphs simultaneously, separated by a SEP token, and truncating the longer of the two first. In that way, we avoid summing and averaging across the sentence and paragraph levels, thus avoiding the loss of information, ultimately leading to better performance.

\section{Dataset}\label{sec:dataset}

As stated in the overview of the task \citep{zangerle2021overview}, the texts were drawn from various Q\&A pages within the StackExchange network. All the texts are technology-oriented. The authors claim to have applied some preprocessing such as removing images, code snippets, URLs, block quotes, and bulleted lists. However, we found some additional things worthy of removal, which will be discussed in subsection \ref{subsec:preprocessing} An author's answer to a question is often split into multiple paragraphs. Within the document, only the same Q\&A thread's paragraphs are shuffled and included, reducing the semantic difference between the paragraphs, thus making the task more challenging. This also means that the same author's paragraphs occur at various places within the document in a non-sequential order.

Each document is between 1,000 and 10,000 characters long. Each paragraph is at least 100 characters long. Train, validation, and test sets are very well balanced. Additional statistics can be found in Table \ref{tab:dataset-statistics}, and some of the examples in Table \ref{tab:dataset-examples}.
\begin{table}
\small
\caption{Dataset statistics. Mean is used for all the statistics, apart from the number of documents. The second column represents number of documents in training, validation, and test set respectively. The following columns represent average number of paragraphs per document, characters per paragraph, and words per paragraph.}
\label{tab:dataset-statistics}
\begin{center}
\begin{tabular}{l c c c c}
\toprule
Split & \#Docs & \#Par./Doc. & \#Char./Par. & \#Words/Par. \\
\midrule
Train & 11,200 & 6.9 & 252.5 & 44.1 \\
Valid. & 2,400 & 6.9 & 253.6 & 44.3 \\
Test & 2,400 & 6.8 & 254.4 & 44.5 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

We had labels for all three tasks at our disposal. For task 1, we knew whether the document was a single- or multi-author one. Task 2 labels indicated if there was an author change at the paragraph transitions since that is the only place where a change can take place. Finally, task 3 assigned an author label to each of the paragraphs within a document. There can be a maximum of 4 authors in the document. We take each pair of the sequential paragraphs and use these as training examples, resulting in $n-1$ new examples from a document with $n$ paragraphs. This resulted in a training set of 30,926 transitions with no author change, and 35,711 in which a change occurred. The validation and test set had an almost identical label distribution as the training one.

\subsection{Preprocessing}\label{subsec:preprocessing}

After examining the examples individually, we realized that there are many containing \LaTeX{} equations, Linux or Windows paths, and URLs. For that matter, we created two additional versions of the dataset -- a cleaned and a non-cleaned one. In a cleaned one, \LaTeX{} commands are replaced with \texttt{latex}, URLs with \texttt{URL} etc. In such a way, the number of examples stays the same. A motivation for identifying Windows and Linux paths and swapping them with \texttt{windows} and \texttt{linux} respectively was that different operating systems could indicate different authors. We used \textit{bert-base-cased}, as we believe that casing could play a significant role in defining the author's style.

\subsection{Extending a Training Set}\label{subsec:extending-a-training-set}

BERT is pre-trained with two objectives -- masked language modeling (MLM) and next sentence prediction (NSP). Although we're dealing with non-sequential paragraphs, we thought it'd be appropriate to use BERT that's pre-trained for the NSP objective and fine-tune it for our task.

Additionally, we decided to extend the training set. In this regard, we pair each paragraph with all of the paragraphs coming after it in the document, yielding $\binom{n}{2}$ examples from a document with $n$ paragraphs. Task 3 labels indicating which paragraph belongs to which author allow us to do this. As a result, we got an extended training set of 125,679 instances with no author change, and 154,082 in which a change occurred. The validation and test set weren't used in the extension. We believed that obtaining more data while keeping a problem formulation the same would be beneficial for the performance. We use this approach for both cleaned and non-cleaned versions.

\section{Approach and Experiments}\label{sec:approach-and-experiments}

As previously mentioned, we used BERT with a classification head on top.\footnote{\url{https://huggingface.co/docs/transformers/model\_doc/bert\#transformers.BertForNextSentencePrediction}}
BERT is a transformer-based architecture which relies on attention \citep{attention-is-all-you-need}.
The head does a linear transformation of the CLS token embedding into 2 dimensions, one for a negative (no change), and the other for a positive (author change) class. Since BERT can take 512 tokens the most at the time, whenever paragraphs' lengths exceeded that, the longer of the two was truncated first. In practice, this happened in around 0.05\% of the examples, as the paragraphs weren't very long (see Table \ref{tab:dataset-statistics}). BERT was given two paragraphs separated by a SEP token, which then acted as a single training example.


Apart from the performance measures, we were also interested in observing how modifying the dataset or the number of parameters being learned affected the model's behavior under this setting. For that matter, we opted for the following models' nomenclature: NENC (Non-Extended Non-Cleaned), NEC (Non-Extended Cleaned), ENC (Extended Non-Cleaned), and EC (Extended Cleaned). Note that the models here are named by the data they were trained on, which isn't standard practice. Each of these was trained 5 times with different seeds. Other parameters are unchanged. Statistical tests presented in Table \ref{tab:statistical-tests} are trying to prove that on a certain fixed train dataset with a fixed set of hyperparameters, a better performance of a model trained on one dataset, compared to the one trained on the other, is not just due to seed randomness. All the models were evaluated on the provided validation and test sets.

Hyperparameter optimization was omitted due to the large training time and an already significant number of experiments. The training was done on a single GeForce GTX 1080 GPU with 11 GB of memory and took 1:15h per epoch for a non-extended and 5h for an extended version. With such resources, the maximum possible batch size was 6. We used \textit{AdamW} \citep{adamw} optimizer and an exponential learning rate scheduler with the initial \emph{learning rate} of 1e-5 and \emph{gamma} of 0.5. The training was done for 5 epochs, but was always stopped early, due to an increase in a validation loss after only a few epochs. A model with the smallest validation loss was taken.

\subsection{Baseline}
The Bernoulli baseline performance is shown in Table \ref{tab:performance}. It classified every example as 1 with a probability equal to the proportion of author changes in the training set.

\begin{table}[h]
\small
\caption{Performance comparison. Results are presented in the form of \emph{mean} $\pm$ \emph{standard deviation} calculated on the sample size of 5 where it was possible. Bold values represent the best results achieved for the metric used. Baseline is compared against previous year's best performing models, which are compared against our work.}
\label{tab:performance}
\begin{center}
\begin{tabular}{l c c}
\toprule
Model & $F_1$ & $F_{0.5}$\\
\midrule
$\text{BASELINE}_{Bernoulli}$ & 0.535 & 0.593 \\
\midrule
\citep{deibel2021style} & 0.669 & - \\
\citep{strom2021multi} & 0.707 & - \\
\citep{zhang2021style} & 0.751 & - \\
\midrule
$\text{BERT}_{NENC}$ & 0.783\pm 0.004 & \textbf{0.792}\pm 0.004 \\
$\text{BERT}_{NEC}$ & 0.794\pm 0.006 & 0.776\pm 0.002 \\
$\text{BERT}_{ENC}$ & \textbf{0.800}\pm 0.005 & 0.783\pm 0.008 \\
$\text{BERT}_{EC}$ & 0.796\pm 0.005 & 0.786\pm 0.008 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

%\FloatBarrier
\begin{table*}[bp]
\small
\caption{Statistical tests. A non-parametric Wilcoxon Signed Rank Test is used with a sample size of 5. Decisions are based on the significance level of 0.05. Symbol \oneS ~denotes that $H_0$ is rejected.}
\label{tab:statistical-tests}
\centering
\begin{tabular}{c c c c}
\toprule
Postulate & $H_0$ & $H_1$ & p-value\\
\midrule
  & $\mu_{EC} = \mu_{NEC}$ & $\mu_{EC} > \mu_{NEC}$ &  \\
1-1 & Extended and non-extended & Extended cleaned dataset performs & 0.500 \\
  & cleaned datasets perform the same & better than a non-extended one &  \\
\midrule
  & $\mu_{ENC} = \mu_{NENC}$ & $\mu_{ENC} > \mu_{NENC}$ &  \\
1-2 & Extended and non-extended & Extended non-cleaned dataset performs & 0.006\oneS \\
  & non-cleaned datasets perform the same & better than a non-extended one &  \\
\midrule
  & $\mu_{NEC} = \mu_{NENC}$ & $\mu_{NEC} > \mu_{NENC}$ &  \\
2-1 & Cleaned and non-cleaned & Cleaned non-extended dataset performs & 0.018\oneS \\
  & non-extended datasets perform the same & better than a non-cleaned one &   \\
\midrule
  & $\mu_{EC} = \mu_{ENC}$ & $\mu_{EC} > \mu_{ENC}$ &  \\
2-2 & Cleaned and non-cleaned & Cleaned extended dataset performs & 0.735 \\
  & extended datasets perform the same & better than a non-cleaned one &  \\
\bottomrule
\end{tabular}
\end{table*}
%\FloatBarrier

\subsection{Extended vs Non-extended}\label{subsec:extended-vs-nonextended}
The dataset was extended as explained in subsection \ref{subsec:extending-a-training-set} We expected an extended dataset to perform better on both cleaned and non-cleaned datasets. The results are shown in Table \ref{tab:performance}, and the statistical tests in Table \ref{tab:statistical-tests} as postulates 1-1 and 1-2. When the dataset is cleaned, EC performs similarly to NEC, and we cannot conclude that extending a dataset helps. On the other side, when the dataset is left unprocessed, ENC outperforms NENC, which is confirmed by a statistical test. With the intuition that extending a dataset is helpful, it appears as if cleaning on the larger dataset causes too much generalization, as the words \texttt{URL, tex, windows}, and \texttt{linux} start appearing in too many different contexts, bringing no additional information, as it would be the case with the whole URLs, paths, or \LaTeX{} equations.

\subsection{Cleaned vs Non-cleaned}
When examining the effect of preprocessing explained in subsection \ref{subsec:preprocessing}, which could also be considered a form of regularization, we expected it to reduce the noise and increase the performance. Models trained on the cleaned training set were also evaluated and tested on the cleaned sets. As shown in Table \ref{tab:performance} and in postulates 2-1 and 2-2 in Table \ref{tab:statistical-tests}, cleaning seems to be helpful only on a non-extended dataset. It appears as if cleaning a non-extended dataset reduces the noise and allows the model to capture certain specificities. A possible explanation as to why cleaning an extended dataset isn't beneficial was given previously in subsection \ref{subsec:extended-vs-nonextended}

\subsection{$\bold{F_{0.5}}$ Metric}
After examining the results, we realized that, despite its worst $F_1$-score, the NENC model had high precision scores. For that reason, we included an $F_{0.5}$ metric in Table \ref{tab:performance} which puts more emphasis on the precision. Interestingly, it ended up having the best $F_{0.5}$-score. There are cases in which the precision is of more importance than the recall, such as plagiarism detection when we want to be sure before accusing someone. In the case of the NENC model, the precision increased at the expense of recall.

%\subsection{Sequential vs Non-sequential}
%After observing the results, we wondered how would a sequential-only dataset act when compared to the one created %from non-sequential examples only. After observing that it performs worse, in postulate 3 of Table %\ref{tab:statistical-tests} we test if this performance is significantly worse and conclude that it is, on both %cleaned and non-cleaned dataset. Only one of the two is presented in the table. This could be attributed to the %way the dataset was constructed, as some of the originally sequential paragraphs were shuffled.

\subsection{Training Different Parameters}
As mentioned in Section \ref{sec:approach-and-experiments}, the training was computationally expensive, but Table \ref{tab:performance} showed an increase in performance for an extended dataset, so we experimented with fine-tuning only parts of the architecture in order to reduce the training time. This experiment was done on NENC. The results are shown in Table \ref{tab:training-different-parameters}. Using a pre-trained BERT as is, gave very bad results because we're dealing with shuffled paragraphs. Fine-tuning biases only is competitive to fine-tuning the whole model, when dealing with small-to-medium-sized training data \citep{bitfit}, so we included that approach.
Fine-tuning a whole BERT appears to be beneficial for author style change detection when the performance and computational resources are taken into account.


\begin{table}
\footnotesize
\caption{Comparison when different parameters are fine-tuned. One pass duration considers time taken in seconds for a forward and backward pass on a single batch. $\Theta$ denotes the number of parameters being fine-tuned. Training was done on a single GeForce GTX 1080 GPU with 11 GB of memory. Sample size is 5.}
\label{tab:training-different-parameters}
\begin{center}
\begin{tabular}{l c c c}
\toprule
Parameters & $\Theta$ & Pass duration ($s$) & $F_1$\\
\midrule
None & - & - & 0.034 \\
Head & 1.5k & 0.045 & 0.637\pm 0.009 \\
Head\&Biases & 104k & 0.283 & 0.744\pm 0.003 \\
Head\&BERT & 110M & 0.343 & 0.783\pm 0.004 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\FloatBarrier
\begin{table*}[b]
\scriptsize
\caption{Test set examples on which the model is confident. Confidence denotes probability of a positive (first and third example) or negative (second and fourth example) class.}
\label{tab:dataset-examples}
\centering
\begin{tabular}{c p{6.7cm} p{6.7cm} c}
\toprule
Example & Paragraph 1 & Paragraph 2 & Confidence\\
\midrule
TP & I am trying to configure basic auth for my index file, and only my index file.  I have configured it like so: & The configuration you posted works perfect on a Debian - Apache 2.2 install. I'd suggest attempting to see if placing it in a \textless Location\textgreater \space directive - and try it on 2 different browsers. & 0.982 \\
\midrule
TN & I also figured that everything still works if I disable DHCP on my TP Link Router and plug the cable in the LAN port. I found this recommendation here. & Should I enable DHCP server on the TP Link Router and should I plug the connection cable between the routers in the WAN or LAN port of my TP Link Router? The other end is in the LAN port of my main router. & 0.932 \\
\midrule
FP & When you want to do more fancy effects, you will probably want to move to WebGL, because the performance of stuff like blending is going to be quite variable (i.e. poor) for 2d canvas. & I wrote such a game and found that it performed very well on most desktop browser (yes - even Internet Explorer). & 0.958 \\
\midrule
FN & I have set up PRTG and it appears that for a server hosting 4 users, the traffic is approximately 100k to 300k per session. Is that about right? & What kind of applications and window resolutions are in use, and how many at once?  Or is it published desktop sessions?  Are there additional local peripherals or other custom session traffic? & 0.953 \\
\bottomrule
\end{tabular}
\end{table*}
\FloatBarrier


\section{Prediction Confidence and Drawbacks}
We were interested in seeing how the model behaves on unseen examples and for which ones it has high confidence.
In Figure~\ref{fig:confidence}, it can be seen how confident the model is in its predictions. The distributions show that only a small fraction of test set examples get classified falsely with high confidence. Most often, when a false classification occurs, the model is uncertain about it. Graphs show desired behavior, as most of the examples get classified correctly with high confidence.

\begin{figure}[h]
\includegraphics[width=8.28cm]{figures/pos_neg_conf_3.png}
\caption{True class probabilities for the examples in which author change didn't (left) or did (right) occur. Probabilities below 0.5 resulted in a false classification. Probabilities closer to 0 or 1 suggest high confidence of a model for these examples.}
\label{fig:confidence}
\end{figure}

In Table \ref{tab:dataset-examples}, we've shown some of the examples for which the model is highly confident. A TP example is a dialogue-like conversation where the first paragraph is in the first person, while the other one starts in the second person. A question-answer structure is also common in TP examples. In fact, 36\% of examples with true class confidence over 0.9 had one of the paragraphs in a question structure, while the other was structured as an answer. A TN example shows a question build-up in the first paragraph, while the second one starts with the actual question. They both discuss the same topic. These examples usually consist of an (originally) single paragraph split and shuffled into multiple ones, which didn't end up too far together. An FP example is written by the same authors, but the two paragraphs aren't really in the same context, as they were shuffled and don't appear that close to each other. This seems to be problematic for the model. Lastly, an FN example answers the question with a question, which our model classifies as no author change. Also, we noticed that two paragraphs, both written in the first person, tend to get classified as FN.



\section{Conclusion and Future Work}
When the dataset is extended, there are indications that the model trained on it performs better, as well as that cleaning becomes disadvantageous. Some of the examples on which a model makes highly-confident mistakes are difficult to classify for a human as well. For an author style change detection problem defined in this setting, fine-tuning all 110M BERT parameters appears to be beneficial.

We believe that extending the dataset could prove useful to increase the model's performance even further. We'd also like to incorporate syntax features.
One of the approaches we tried was a CNN-BiLSTM architecture with triplet margin loss. It didn't produce good results, but we believe that the approach is right and would like to explore it further in the future.
Additionally, BiLSTM was tried for sequence labeling, where a sequence instance was a single paragraph within the document. It also didn't yield good results, but we'd like to explore it further for task 3 of the competition.
Finally, one of the possible critiques could be that we're unable to deal with longer paragraphs in the current setting. We found an approach of extending the dataset by splitting the paragraph into sentences and feeding a model with two of them at the time compelling and we're interested in seeing the results it would produce.

\section*{Acknowledgements}

We'd like to thank the organizers of PAN at CLEF 2021 Author Style Change Detection task for sharing a test dataset with us. 

\bibliographystyle{tar2022}
\bibliography{tar2022} 

\end{document}