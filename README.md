# TAR Project, FER, 2022.

## Task: Author Style Change Detection CLEF 2021

Members:
* Andreis, Marko
* Ferković, Tin
* Martinović, Ivan

Branches:
* main - Code needed for experiments reproduction
* cnn_bilstm_task2 - our attempt to learn character-level embeddings to encode syntactic features
* task3_bilstm - our attempt to solve 3. task with sequence labelling (LSTM)
* feature_extraction - features extraction from each paragraph and then classifier on top


/datasets - contains cleaned and extended datasets and different statistics

/src - train scripts

/notebooks - some notebooks for data exploration and statistical tests