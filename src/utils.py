import torch
import torch.nn as nn
import datetime
from sklearn.metrics import (
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
    confusion_matrix,
)


class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, df):
        self.first_texts = df["first_text"].values.tolist()
        self.second_texts = df["second_text"].values.tolist()
        self.author_changes = df["author_change"].values.tolist()

    def __len__(self):
        return len(self.first_texts)

    def __getitem__(self, idx):
        return self.first_texts[idx], self.second_texts[idx], self.author_changes[idx]


class Discriminator(nn.Module):
    def __init__(
        self, dropout, input_size=768, hidden_sizes=[1024, 256], output_size=1
    ):
        super().__init__()
        layers = [
            nn.Dropout(p=dropout),
            nn.Linear(input_size, hidden_sizes[0]),
            nn.BatchNorm1d(hidden_sizes[0]),
            nn.ReLU(),
            nn.Dropout(p=dropout),
        ]
        for i in range(len(hidden_sizes) - 1):
            layers.extend(
                [
                    nn.Linear(hidden_sizes[i], hidden_sizes[i + 1]),
                    nn.BatchNorm1d(hidden_sizes[i + 1]),
                    nn.ReLU(),
                    nn.Dropout(p=dropout),
                ]
            )
        layers.extend([nn.Linear(hidden_sizes[-1], output_size)])
        self.layers = nn.Sequential(*layers)
        self.bce_with_logits = nn.BCEWithLogitsLoss()

    def forward(self, input):
        return self.layers(input)

    def loss(self, logits, target):
        return self.bce_with_logits(logits, target)


def invert_labels(labels):
    return 1 - labels


def format_seconds(seconds):
    td = datetime.timedelta(seconds=int(round(seconds)))
    return str(td)


def calculate_scores(y_true, y_pred):
    accuracy = accuracy_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    conf_matrix = confusion_matrix(y_true, y_pred)

    return accuracy, f1, precision, recall, conf_matrix
