import torch
from transformers import BertForNextSentencePrediction, BertTokenizer
import os
import pandas as pd
from task_2_using_CLS import CustomDataset
import pickle
from tqdm import tqdm
from utils import calculate_scores

# change to 0 or 1, depending on which GPU you want to use
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")


MODELS_DIR = f"models/non_massive_experiment_mali_lr1e-5_non_cleaned_scheduler_09_try1"

print(f"Testing experiment: {MODELS_DIR}")

PATH = os.path.join(
    MODELS_DIR, "cleaned_non_massive_ds_NSP_best_model_on_validation.pt"
)

tokenizer = BertTokenizer.from_pretrained("bert-base-cased")
bert = BertForNextSentencePrediction.from_pretrained("bert-base-cased")
checkpoint = torch.load(PATH)
bert.load_state_dict(checkpoint["bert_state_dict"])
bert = bert.to(device)
bert.eval()

test_df = pd.read_csv("datasets/test.csv")

test_data_loader = torch.utils.data.DataLoader(
    CustomDataset(test_df), shuffle=False, batch_size=1
)

y_true = []
y_pred = []
results = {}
with torch.no_grad():
    for i, (first_texts, second_texts, author_changes) in enumerate(
        tqdm(test_data_loader, total=len(test_data_loader))
    ):
        author_changes = author_changes.to(device)

        encoding = tokenizer(
            first_texts,
            second_texts,
            padding="max_length",
            truncation="longest_first",
            return_tensors="pt",
        ).to(device)

        outputs = bert(**encoding, labels=author_changes)
        logits = outputs.logits

        y_pred.extend(torch.argmax(logits, dim=1).detach().cpu())
        y_true.extend(torch.reshape(author_changes, (-1,)).float().detach().cpu())

        outs = torch.nn.functional.softmax(logits)
        results[i] = {
            "first_text": first_texts,
            "second_text": second_texts,
            "label": author_changes[0].item(),
            "0": outs[0][0].item(),
            "1": outs[0][1].item(),
        }

with open("results.pickle", "wb") as handle:
    pickle.dump(results, handle, protocol=pickle.HIGHEST_PROTOCOL)

# with open('filename.pickle', 'rb') as handle:
#     b = pickle.load(handle)

y_true = torch.stack(y_true).numpy()
y_pred = torch.stack(y_pred).numpy()

accuracy, f1, precision, recall, conf_matrix = calculate_scores(y_true, y_pred)

print("Accuracy:", accuracy)
print("F1-score:", f1)
print("Precision:", precision)
print("Recall:", recall)
print("Confusion matrix:")
print(conf_matrix)

with open(os.path.join(MODELS_DIR, "logs.txt"), "a") as f:
    f.write(f"{'='*100}")
    f.write("\n\nTEST PERFORMANCE\n")
    f.write(f"Accuracy: {accuracy}\n")
    f.write(f"F1-score: {f1}\n")
    f.write(f"Precision: {precision}\n")
    f.write(f"Recall: {recall}\n")
    f.write(f"Confusion matrix:\n")
    f.write(f"{str(conf_matrix)}")
